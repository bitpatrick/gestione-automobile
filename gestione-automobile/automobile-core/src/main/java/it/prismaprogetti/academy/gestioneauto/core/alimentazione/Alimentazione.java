package it.prismaprogetti.academy.gestioneauto.core.alimentazione;

import java.util.Optional;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;

public class Alimentazione {

	private CodiceAlimentazione codiceAlimentazione;
	private NomeAlimentazione nomeAlimentazione;

	private Alimentazione(CodiceAlimentazione codiceAlimentazione, NomeAlimentazione nomeAlimentazione) {
		super();
		this.codiceAlimentazione = codiceAlimentazione;
		this.nomeAlimentazione = nomeAlimentazione;
	}

	public CodiceAlimentazione getCodiceAlimentazione() {
		return codiceAlimentazione;
	}

	public NomeAlimentazione getNomeAlimentazione() {
		return nomeAlimentazione;
	}
	
	public static Alimentazione creaAlimentazione(CodiceAlimentazione codiceAlimentazione, NomeAlimentazione nomeAlimentazione) throws NoSuchParamException {
		
		CodiceAlimentazione.checkNotNull(codiceAlimentazione);
		NomeAlimentazione.checkNotNull(nomeAlimentazione);
		
		return new Alimentazione(codiceAlimentazione, nomeAlimentazione);
		
	}
	
	public static void checkIsNotNull(Alimentazione alimentazione) throws AlimentazioneNotFoundException {
		
		if ( alimentazione == null ) {
			throw new AlimentazioneNotFoundException("alimentazione is null");
		}
	}
	
	public static void checkIfExistsAlimentazione(CodiceAlimentazione codiceAlimentazione, AlimentazioneRepository alimentazioneRepository)
			throws NoSuchParamException, AlimentazioneNotFoundException {

		Optional<Alimentazione> alimentazioneOpt = alimentazioneRepository.trovaPerCodice(codiceAlimentazione);

		if (alimentazioneOpt.isEmpty()) {
			throw new AlimentazioneNotFoundException("alimentazione non presente nel repository");
		}

	}

}
