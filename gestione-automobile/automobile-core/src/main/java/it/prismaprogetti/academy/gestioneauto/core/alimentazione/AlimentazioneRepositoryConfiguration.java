package it.prismaprogetti.academy.gestioneauto.core.alimentazione;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;
import it.prismaprogetti.academy.gestioneauto.core.impl.InMemoryAlimentazioneRepository;
import it.prismaprogetti.academy.gestioneauto.core.modello.Modello;
import it.prismaprogetti.academy.gestioneauto.core.modello.ModelloGiaPresenteException;
import it.prismaprogetti.academy.gestioneauto.core.modello.ModelloRepository;

public class AlimentazioneRepositoryConfiguration {

	private String percorsoFileDelleAlimentazioni;
	private String percorsoFileDelleAlimentazioniPerModelli;

	public AlimentazioneRepositoryConfiguration(String percorsoFileDelleAlimentazioni,
			String percorsoFileDelleAlimentazioniPerModelli) {
		super();
		this.percorsoFileDelleAlimentazioni = percorsoFileDelleAlimentazioni;
		this.percorsoFileDelleAlimentazioniPerModelli = percorsoFileDelleAlimentazioniPerModelli;
	}

	/**
	 * inserisco le alimentazioni nel db
	 * 
	 * @param separatoreDiCampo
	 * @param marcatoreFineRiga
	 * @return
	 * @throws NoSuchParamException
	 * @throws AlimentazioneNotFoundException
	 * @throws AlimentazioneGiaPresenteException
	 */
	public AlimentazioneRepository configureFileDelleAlimentazioni(char separatoreDiCampo, char marcatoreFineRiga) {

		/*
		 * istanzio il lettore del tipo di alimentazione
		 */
		AlimentazioneReader alimentazioneReader = new AlimentazioneReader(separatoreDiCampo, marcatoreFineRiga);

		List<Alimentazione> alimentazioni = alimentazioneReader.read(this.percorsoFileDelleAlimentazioni);

		AlimentazioneRepository alimentazioneRepository = new InMemoryAlimentazioneRepository();

		for (Alimentazione alimentazione : alimentazioni) {

			try {
				alimentazioneRepository.salva(alimentazione);

			} catch (Exception e) {
				e.getMessage();
			}

		}

		return alimentazioneRepository;

	}

	public void configureFileDelleAlimentazioniPerModelli(char separatoreDiCampo, char marcatoreFineRiga,
			ModelloRepository modelloRepository, AlimentazioneRepository alimentazioneRepository) throws NoSuchParamException, AlimentazioneNotFoundException, AlimentazioneGiaPresenteException {

		AlimentazionePerModelliReader alimentazionePerModelliReader = new AlimentazionePerModelliReader(
				separatoreDiCampo, marcatoreFineRiga);

		Set<AlimentazionePerModello> alimentazioniPerModelli = alimentazionePerModelliReader
				.read(this.percorsoFileDelleAlimentazioniPerModelli);

		Set<Modello> modelliModificati = new HashSet<>();

		for (AlimentazionePerModello alimentazionePerModello : alimentazioniPerModelli) {

			/*
			 * tento di recuperare il codice modello dal repository
			 */
			Optional<Modello> modelloOptional = modelloRepository
					.trovaPerCodice(alimentazionePerModello.getCodiceModello());

			/*
			 * tento di recuperare l'alimentazione dal repository
			 */
			Optional<Alimentazione> alimentazioneOptional = alimentazioneRepository
					.trovaPerCodice(alimentazionePerModello.getCodiceAlimentazione());

			if (modelloOptional.isEmpty()) {
				System.err.println("modello is empty");
				continue;
			}

			if (alimentazioneOptional.isEmpty()) {
				System.err.println("alimentazione is empty");
				continue;
			}

			Modello modello = modelloOptional.get();
			Alimentazione alimentazione = alimentazioneOptional.get();
			/*
			 * imposto alimentazione nel modello corrispondente
			 */
			modello.aggiungiAlimentazione(alimentazione);

			modelliModificati.add(modello);

		}

		/*
		 * salvo solo i modelli modificati
		 */
		for (Modello modello : modelliModificati) {

			try {
				modelloRepository.salva(modello);
			} catch (ModelloGiaPresenteException | NoSuchParamException e) {

				System.err.println("salto modello da salvare: " + e.getMessage());
			}

		}

	}

}
