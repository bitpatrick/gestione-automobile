package it.prismaprogetti.academy.gestioneauto.core.exceptions;

public class NoSuchParamException extends Exception {

	public NoSuchParamException(String message) {
		super(message);
	}

}
