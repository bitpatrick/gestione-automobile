package it.prismaprogetti.academy.gestioneauto.core.modello;

public class ModelloGiaPresenteException extends Exception {

	public ModelloGiaPresenteException(String message) {
		super(message);
	}

}
