package it.prismaprogetti.academy.gestioneauto.core.marca;

import java.util.List;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;
import it.prismaprogetti.academy.gestioneauto.core.impl.InMemoryMarcaRepository;

/**
 * componente che prepara il repository delle marche
 * 
 * @author patri
 *
 */
public class MarcaRepositoryConfiguration {

	private String percorsoFileDelleMarche;

	public MarcaRepositoryConfiguration(String percorsoFileDelleMarche) {
		super();
		this.percorsoFileDelleMarche = percorsoFileDelleMarche;
	}

	/**
	 * Questo metodo imposta, popola e restituisce un repository
	 * 
	 * @param separatoreDiCampo
	 * @param marcatoreFineRiga
	 * @return un'istanza di InMemoryMarcaRepository che implementa MarcaRepository
	 * @throws MarcaGiaPresenteException 
	 * @throws NoSuchParamException 
	 */
	public MarcaRepository configure(char separatoreDiCampo, char marcatoreFineRiga) {

		/*
		 * istanzio il lettore delle marche
		 */
		MarcaReader marcaReader = new MarcaReader(separatoreDiCampo, marcatoreFineRiga);

		/*
		 * applico il seguete metodo del suddetto lettore per ottenere una lista di
		 * marche
		 */
		List<Marca> marche = marcaReader.read(this.percorsoFileDelleMarche);

		MarcaRepository marcaRepository = new InMemoryMarcaRepository();

		for (Marca marca : marche) {
			
			try {
				marcaRepository.salva(marca);
			} catch (MarcaGiaPresenteException | NoSuchParamException e) {
				System.err.println("fallito salvataggio marca: " + e.getMessage());
			}
			
		}

		return marcaRepository;
	}

}
