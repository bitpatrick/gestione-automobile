package it.prismaprogetti.academy.gestioneauto.core.alimentazione;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;
import it.prismaprogetti.academy.gestioneauto.core.modello.CodiceModello;

public class AlimentazionePerModelliReader {

	private final char SEPARATORE_DI_CAMPO;
	private final char MARCATORE_FINE_RIGA;

	protected AlimentazionePerModelliReader(char sEPARATORE_DI_CAMPO, char mARCATORE_FINE_RIGA) {
		super();
		SEPARATORE_DI_CAMPO = sEPARATORE_DI_CAMPO;
		MARCATORE_FINE_RIGA = mARCATORE_FINE_RIGA;

	}

	protected Set<AlimentazionePerModello> read(String percorsoDelFile) throws NoSuchParamException {

		Set<AlimentazionePerModello> listaModelliPerAlimentazione = new HashSet<>();

		try (
				FileReader fileReader = new FileReader(percorsoDelFile);
				BufferedReader bufferedReader = new BufferedReader(fileReader);
			) {

			String riga = null;

			while ((riga = bufferedReader.readLine()) != null) {

				/*
				 * Verificare che la riga abbia il marcatore fine riga ";"
				 */
				if (!riga.endsWith(String.valueOf(MARCATORE_FINE_RIGA))) {
					System.err.println(
							"La riga: " + riga + " verr� scartata perch� sprovvista del seguente marcatore fine riga ["
									+ this.MARCATORE_FINE_RIGA + "]");
					continue;
				}

				riga = riga.replaceAll(String.valueOf(this.MARCATORE_FINE_RIGA), "");
				String[] rigaArray = riga.split(String.valueOf(this.SEPARATORE_DI_CAMPO));

				/*
				 * verifico che l'array abbia al suo interno 2 elementi
				 */
				if (rigaArray.length != 2) {
					System.err.println(" L'array ha una lunghezza invalida, per questo verr� scartato");
					continue;
				}

				CodiceModello codiceModello = CodiceModello.creaOrNull(rigaArray[0]);
				CodiceAlimentazione codiceAlimentazione = CodiceAlimentazione.creaOrNull(rigaArray[1]);
				

				AlimentazionePerModello alimentazionePerModello = AlimentazionePerModello.crea(codiceAlimentazione,
						codiceModello);

				listaModelliPerAlimentazione.add(alimentazionePerModello);

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listaModelliPerAlimentazione;

	}

}
