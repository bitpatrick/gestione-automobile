package it.prismaprogetti.academy.gestioneauto.core.modello;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;

public class NomeModello {
	
	private String nome;

	private NomeModello(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	@Override
	public int hashCode() {

		return this.getNome().hashCode();
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}

		if (!(obj instanceof NomeModello)) {

			return false;
		}

		return ((NomeModello) obj).getNome().equals(this.getNome());
	}

	public static void checkNotNull(NomeModello NomeModello) throws NoSuchParamException {

		if (NomeModello == null) {
			throw new NoSuchParamException("il nome del modello � nullo");
		}

	}

	public static NomeModello crea(String nome) throws NoSuchParamException {

		if (nome == null || nome.trim().isEmpty()) {
			throw new NoSuchParamException("nome marca nullo");
		}

		return new NomeModello(nome.toUpperCase());
	}

	public static NomeModello creaOrNull(String nome) {

		NomeModello NomeModello = null;

		try {
			NomeModello = crea(nome);

		} catch (NoSuchParamException e) {
		}

		return NomeModello;
	}

}
