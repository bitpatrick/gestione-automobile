package it.prismaprogetti.academy.gestioneauto.core.alimentazione;

import java.util.List;
import java.util.Optional;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;
import it.prismaprogetti.academy.gestioneauto.core.marca.MarcaGiaPresenteException;

public interface AlimentazioneRepository {
	
	void salva(Alimentazione alimentazione) throws NoSuchParamException, AlimentazioneNotFoundException, AlimentazioneGiaPresenteException;
	Optional<Alimentazione> trovaPerCodice(CodiceAlimentazione codiceAlimentazione) throws NoSuchParamException, AlimentazioneNotFoundException;
	List<Alimentazione> recupera();

}
