package it.prismaprogetti.academy.gestioneauto.core.modello;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;

public class CodiceModello {

	private String codice;

	private CodiceModello(String codice) {
		super();
		this.codice = codice;
	}
	
	public String getCodice() {
		return codice;
	}

	public static CodiceModello crea(String codice) throws NoSuchParamException {
		if (codice == null || codice.trim().isEmpty()) {
			throw new NoSuchParamException("Codice modello nullo");
		}

		return new CodiceModello(codice.toUpperCase());
	}

	public static CodiceModello creaOrNull(String codice) {

		CodiceModello codiceModello = null;

		try {
			codiceModello = crea(codice);

		} catch (NoSuchParamException e) {
	}

		return codiceModello;
	}

	public static void checkNotNull(CodiceModello codiceModello) throws NoSuchParamException {

		if (codiceModello == null) {
			throw new NoSuchParamException("il codice modello � nullo");
		}

	}

	@Override
	public int hashCode() {
		return this.codice.hashCode();
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}

		if (!(obj instanceof CodiceModello)) {
			return false;
		}
		

		return ((CodiceModello) obj).codice.equals(this.codice);
	}

	

}
