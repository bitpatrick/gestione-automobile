package it.prismaprogetti.academy.gestioneauto.core.modello;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;
import it.prismaprogetti.academy.gestioneauto.core.marca.CodiceMarca;
import it.prismaprogetti.academy.gestioneauto.core.marca.MarcaNotFoundException;
import it.prismaprogetti.academy.gestioneauto.core.marca.MarcaRepository;

public class ModelloReader {

	private final char SEPARATORE_DI_CAMPO;
	private final char MARCATORE_FINE_RIGA;
	private final MarcaRepository marcaRepository;

	protected ModelloReader(char sEPARATORE_DI_CAMPO, char mARCATORE_FINE_RIGA, MarcaRepository marcaRepository) {
		super();
		SEPARATORE_DI_CAMPO = sEPARATORE_DI_CAMPO;
		MARCATORE_FINE_RIGA = mARCATORE_FINE_RIGA;
		this.marcaRepository = marcaRepository;
	}

	/**
	 * Metodo per la lettera dei file
	 * 
	 * @param percorsoDelFile
	 * @return una Lista delle Marche presenti al rispettivo percorso del file
	 */
	public List<Modello> read(String percorsoDelFile) {
		
		List<Modello> listaModelli = new ArrayList<>();

		try (
				FileReader fileReader = new FileReader(percorsoDelFile);
				BufferedReader bufferedReader = new BufferedReader(fileReader);
			) {

			String riga = null;

			/*
			 * Costrutto che mi permette di ciclare sul file
			 */
			while ((riga = bufferedReader.readLine()) != null) {
				
				/*
				 * Verificare che la riga abbia il marcatore fine riga ";" ;
				 */
				if (!riga.endsWith(String.valueOf(this.MARCATORE_FINE_RIGA))) {
					System.err.println(
							"La riga: " + riga + " verr� scartata perch� sprovvista del seguente marcatore fine riga ["
									+ this.MARCATORE_FINE_RIGA + "]");
					continue;
				}

				/*
				 * tento di creare l'array contenente
				 * [codiceMarca,codiceModello,nomeModello,urlModello]
				 */
				String[] rigaArray = riga.split(String.valueOf(this.SEPARATORE_DI_CAMPO));
				

				/*
				 * verifico che l'array abbia al suo interno 4 elementi
				 */
				if (rigaArray.length != 4) {
					System.err.println("L'array ha una lunghezza invalida, per questo verr� scartato");
					continue;
				}

				CodiceMarca codiceMarca = CodiceMarca.creaOrNull(rigaArray[0].trim().toUpperCase());
				CodiceModello codiceModello = CodiceModello.creaOrNull(rigaArray[1].trim().toUpperCase());
				NomeModello nomeModello = NomeModello.creaOrNull(rigaArray[2].trim().toUpperCase());
				UrlModello urlModello = UrlModello
						.creaOrNull(rigaArray[3].trim().replaceAll(String.valueOf(this.MARCATORE_FINE_RIGA), ""));

				try {
					
					Modello modello = Modello.creazioneModello(codiceMarca, codiceModello, nomeModello, urlModello, marcaRepository);
					
					listaModelli.add(modello);

				} catch (NoSuchParamException | MarcaNotFoundException e) {
					System.err.println("Record del modello [" + riga + "]" + " non valido: " + e.getMessage());
				}

			}

		} catch (IOException e) {
			e.printStackTrace();

		}
		return listaModelli;

	}

}
