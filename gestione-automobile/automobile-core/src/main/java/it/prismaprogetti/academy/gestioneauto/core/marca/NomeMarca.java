package it.prismaprogetti.academy.gestioneauto.core.marca;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;

public class NomeMarca {

	private String nome;

	private NomeMarca(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	@Override
	public int hashCode() {

		return this.getNome().hashCode();
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}

		if (!(obj instanceof NomeMarca)) {

			return false;
		}

		return ((NomeMarca) obj).getNome().equals(this.getNome());
	}

	public static void checkNotNull(NomeMarca nomeMarca) throws NoSuchParamException {

		if (nomeMarca == null) {
			throw new NoSuchParamException("il nome della marca � nullo");
		}

	}

	public static NomeMarca crea(String nome) throws NoSuchParamException {

		if (nome == null || nome.trim().isEmpty()) {
			throw new NoSuchParamException("nome marca nullo");
		}

		return new NomeMarca(nome.toUpperCase());
	}

	public static NomeMarca creaOrNull(String nome) {

		NomeMarca nomeMarca = null;

		try {
			nomeMarca = crea(nome);

		} catch (NoSuchParamException e) {
		}

		return nomeMarca;
	}

}
