package it.prismaprogetti.academy.gestioneauto.core.modello;

import java.util.List;
import java.util.Optional;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;

public interface ModelloRepository {

	Optional<Modello> trovaPerCodice(CodiceModello codiceModello) throws NoSuchParamException;

	List<Modello> recupera();

	void salva(Modello modello) throws ModelloGiaPresenteException, NoSuchParamException;

}
