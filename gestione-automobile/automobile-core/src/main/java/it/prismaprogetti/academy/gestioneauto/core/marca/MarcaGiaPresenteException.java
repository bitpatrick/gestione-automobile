package it.prismaprogetti.academy.gestioneauto.core.marca;

public class MarcaGiaPresenteException extends Exception {

	public MarcaGiaPresenteException(String message) {
		super(message);
	}

}
