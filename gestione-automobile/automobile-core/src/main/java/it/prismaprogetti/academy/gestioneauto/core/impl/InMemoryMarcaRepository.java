package it.prismaprogetti.academy.gestioneauto.core.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;
import it.prismaprogetti.academy.gestioneauto.core.marca.CodiceMarca;
import it.prismaprogetti.academy.gestioneauto.core.marca.Marca;
import it.prismaprogetti.academy.gestioneauto.core.marca.MarcaGiaPresenteException;
import it.prismaprogetti.academy.gestioneauto.core.marca.MarcaRepository;
import it.prismaprogetti.academy.gestioneauto.core.marca.NomeMarca;

/**
 * Classe che funge da strato di persistenza dei dati
 */
public class InMemoryMarcaRepository implements MarcaRepository {

	private Map<CodiceMarca, Marca> marcaByCodice = new HashMap<>();
	private Map<NomeMarca, Marca> marcaByNomeMarca = new HashMap<>();

	public void salva(Marca marca) throws MarcaGiaPresenteException, NoSuchParamException {

		if (marcaByCodice.get(marca.getCodiceMarca()) != null) {
			throw new MarcaGiaPresenteException(
					"impossibile salvare la marca perch� gi� presente per il codice " + marca.getCodiceMarca().getCodice());
		}

		marcaByCodice.put(marca.getCodiceMarca(), marca);

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		if (marcaByNomeMarca.get(marca.getNomeMarca()) != null) {
			throw new MarcaGiaPresenteException(
					"impossibile salvare la marca perch� gi� presente per il nome " + marca.getNomeMarca().getNome());
		}

		marcaByNomeMarca.put(marca.getNomeMarca(), marca);
	}

	/**
	 * Metodo che restituisce un'istanza della classe Optional contenente
	 * eventualemnte un'istanza della classe Marca
	 * 
	 * @throws NoSuchParamException
	 */
	public Optional<Marca> trovaPerCodice(CodiceMarca codiceMarca) throws NoSuchParamException {

		CodiceMarca.checkNotNull(codiceMarca);
				
		Marca marca = marcaByCodice.get(codiceMarca);
		Marca.checkNotNull(marca);
		
		return Optional.of(marca);
	}

	/**
	 * Metodo che recupera tutte le marche in una lista
	 */
	public List<Marca> recupera() {

		return new ArrayList<Marca>(marcaByCodice.values());
		// return (List<Marca>) listaMarcheByNome.values(); minuto 36
	}

}
