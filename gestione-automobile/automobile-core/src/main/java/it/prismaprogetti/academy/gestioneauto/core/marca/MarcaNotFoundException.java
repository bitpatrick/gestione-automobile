package it.prismaprogetti.academy.gestioneauto.core.marca;

public class MarcaNotFoundException extends Exception {

	public MarcaNotFoundException(String message) {
		super(message);
	}
	

}
