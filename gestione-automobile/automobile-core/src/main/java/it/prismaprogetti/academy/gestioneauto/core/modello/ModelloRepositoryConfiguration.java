package it.prismaprogetti.academy.gestioneauto.core.modello;

import java.util.List;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;
import it.prismaprogetti.academy.gestioneauto.core.impl.InMemoryModelloRepository;
import it.prismaprogetti.academy.gestioneauto.core.marca.MarcaGiaPresenteException;
import it.prismaprogetti.academy.gestioneauto.core.marca.MarcaRepository;

public class ModelloRepositoryConfiguration {

	private String percorsoFileDeiModelli;

	public ModelloRepositoryConfiguration(String percorsoFileDelleMarche) {
		super();
		this.percorsoFileDeiModelli = percorsoFileDelleMarche;
	}

	/**
	 * Questo metodo imposta, popola e restituisce un repository
	 * 
	 * @param separatoreDiCampo
	 * @param marcatoreFineRiga
	 * @return un'istanza di InMemoryMarcaRepository che implementa MarcaRepository
	 * @throws MarcaGiaPresenteException
	 * @throws NoSuchParamException
	 */
	public ModelloRepository configure(char separatoreDiCampo, char marcatoreFineRiga, MarcaRepository marcaRepository) {

		/*
		 * istanzio il lettore delle marche
		 */
		ModelloReader modelloReader = new ModelloReader(separatoreDiCampo, marcatoreFineRiga, marcaRepository);

		/*
		 * applico il seguete metodo del suddetto lettore per ottenere una lista
		 */
		List<Modello> listaModelli = modelloReader.read(this.percorsoFileDeiModelli);
		
		/*
		 * creazione repository per modelli
		 */
		ModelloRepository modelloRepository = new InMemoryModelloRepository();

		for (Modello modello : listaModelli) {

			try {
				modelloRepository.salva(modello);
			} catch (ModelloGiaPresenteException | NoSuchParamException e) {
				System.err.println("salto modello da salvare: " + e.getMessage());
			}
		}

		return modelloRepository;
	}

}
