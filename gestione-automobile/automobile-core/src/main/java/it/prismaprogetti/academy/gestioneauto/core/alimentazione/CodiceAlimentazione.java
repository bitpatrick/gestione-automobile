package it.prismaprogetti.academy.gestioneauto.core.alimentazione;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;

public class CodiceAlimentazione {

	private int codice;

	protected CodiceAlimentazione(int codice) {
		super();
		this.codice = codice;
	}

	public int getCodice() {
		return codice;
	}
	
	public static CodiceAlimentazione creaOrNull(String codiceInput) {

		int codice = 0;

		try {
			codice = (int) Integer.parseInt(codiceInput.toUpperCase());
		} catch (NumberFormatException | ClassCastException e) {
			e.printStackTrace();
		}

		return new CodiceAlimentazione(codice);

	}
	
	public static void checkNotNull(CodiceAlimentazione codiceAlimentazione) throws NoSuchParamException {

		if (codiceAlimentazione == null) {
			throw new NoSuchParamException("il codice alimentazione � nullo");
		}

	}
	
	@Override
	public int hashCode() {
		return this.codice;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}

		if (!(obj instanceof CodiceAlimentazione)) {
			return false;
		}

		return ((CodiceAlimentazione) obj).codice == this.codice;
	}

}
