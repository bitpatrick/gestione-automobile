package it.prismaprogetti.academy.gestioneauto.core.marca;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;

/**
 * Astrazione concetto di codice automobile
 * 
 * @author patri
 *
 */
public class CodiceMarca {

	private String codice;

	private CodiceMarca(String codice) {
		super();
		this.codice = codice;
	}

	public static CodiceMarca crea(String codice) throws NoSuchParamException {
		if (codice == null || codice.trim().isEmpty()) {
			throw new NoSuchParamException("Codice marca nullo");
		}

		return new CodiceMarca(codice.toUpperCase());
	}

	public static CodiceMarca creaOrNull(String codice) {

		CodiceMarca codiceMarca = null;

		try {
			codiceMarca = crea(codice);

		} catch (NoSuchParamException e) {}

		return codiceMarca;
	}

	public static void checkNotNull(CodiceMarca codiceMarca) throws NoSuchParamException {

		if (codiceMarca == null) {
			throw new NoSuchParamException("il codice marca � nullo");
		}

	}

	@Override
	public int hashCode() {
		return this.codice.hashCode();
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}

		if (!(obj instanceof CodiceMarca)) {
			return false;
		}

		return ((CodiceMarca) obj).codice.equals(this.codice);
	}

	public String getCodice() {
		return codice;
	}

}
