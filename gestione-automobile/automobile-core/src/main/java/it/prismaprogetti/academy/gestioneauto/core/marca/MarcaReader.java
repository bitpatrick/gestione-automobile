package it.prismaprogetti.academy.gestioneauto.core.marca;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;

/**
 * Componente per leggere le marche all'interno di un file di testo
 * 
 * @author patri
 *
 */
public class MarcaReader {

	private final char SEPARATORE_DI_CAMPO;
	private final char MARCATORE_FINE_RIGA;

	protected MarcaReader(char sEPARATORE_DI_CAMPO, char mARCATORE_FINE_RIGA) {
		super();
		SEPARATORE_DI_CAMPO = sEPARATORE_DI_CAMPO;
		MARCATORE_FINE_RIGA = mARCATORE_FINE_RIGA;
	}

	/**
	 * Metodo per la lettera dei file
	 * 
	 * @param percorsoDelFile
	 * @return una Lista delle Marche presenti al rispettivo percorso del file
	 */
	public List<Marca> read(String percorsoDelFile) {

		List<Marca> listaMarche = new ArrayList<Marca>();

		try (
				FileReader fileReader = new FileReader(percorsoDelFile);
				BufferedReader bufferedReader = new BufferedReader(fileReader);
			) {

			String riga = null;
			
			/*
			 * Costrutto che mi permette di ciclare sul file
			 */
			while ((riga = bufferedReader.readLine()) != null) {

				/*
				 * Verificare che la riga abbia il marcatore fine riga ";" ;
				 */
				if (!riga.endsWith(String.valueOf(this.MARCATORE_FINE_RIGA))) {
					System.err.println(
							"La riga: " + riga + " verr� scartata perch� sprovvista del seguente marcatore fine riga [" + this.MARCATORE_FINE_RIGA + "]" );
					continue;
				}

				/*
				 * tento di creare l'array contenente [codice,nome,url]
				 */
				String[] rigaArray = riga.split(String.valueOf(this.SEPARATORE_DI_CAMPO));

				/*
				 * verifico che l'array abbia al suo interno 3 elementi
				 */
				if (rigaArray.length < 3 || rigaArray.length > 3) {
					System.err.println("L'array ha una lunghezza invalida, per questo verr� scartato");
					continue;
				}

				CodiceMarca codiceMarca = CodiceMarca.creaOrNull(rigaArray[0].trim().toUpperCase());
				NomeMarca nomeMarca = NomeMarca.creaOrNull(rigaArray[1].trim().toUpperCase());
				UrlMarca urlMarca = UrlMarca.creaOrNull(rigaArray[2].trim().replaceAll(String.valueOf(this.MARCATORE_FINE_RIGA), ""));
				
				try {

					Marca marca = Marca.creazioneMarca(codiceMarca, nomeMarca, urlMarca);

					listaMarche.add(marca);

				} catch (NoSuchParamException e) {

					System.err.println("Record della marca [" + riga + "]" + " non valido: " + e.getMessage());
				}

			}

		} catch (IOException e) {
			e.printStackTrace();

		}
		return listaMarche;

	}
}
