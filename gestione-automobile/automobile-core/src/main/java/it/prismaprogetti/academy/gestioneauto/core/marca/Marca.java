package it.prismaprogetti.academy.gestioneauto.core.marca;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;

public class Marca {

	private CodiceMarca codiceMarca;
	private NomeMarca nomeMarca;
	private UrlMarca urlMarca;

	private Marca(CodiceMarca codiceMarca, NomeMarca nomeMarca, UrlMarca urlMarca) {
		super();
		this.codiceMarca = codiceMarca;
		this.nomeMarca = nomeMarca;
		this.urlMarca = urlMarca;
	}

	public CodiceMarca getCodiceMarca() {
		return codiceMarca;
	}

	public NomeMarca getNomeMarca() {
		return nomeMarca;
	}

	public UrlMarca getUrlMarca() {
		return urlMarca;
	}

	public static Marca creazioneMarca(CodiceMarca codiceMarca, NomeMarca nomeMarca, UrlMarca urlMarca)
			throws NoSuchParamException {

		CodiceMarca.checkNotNull(codiceMarca);
		NomeMarca.checkNotNull(nomeMarca);

		return new Marca(codiceMarca, nomeMarca, urlMarca);
	}
	
	public static void checkNotNull(Marca marca) throws NoSuchParamException {

		if (marca == null) {
			throw new NoSuchParamException("La marca � nulla");
		}

	}

}
