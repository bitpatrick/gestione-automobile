package it.prismaprogetti.academy.gestioneauto.core.modello;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import it.prismaprogetti.academy.gestioneauto.core.alimentazione.Alimentazione;
import it.prismaprogetti.academy.gestioneauto.core.alimentazione.AlimentazioneGiaPresenteException;
import it.prismaprogetti.academy.gestioneauto.core.alimentazione.AlimentazioneNotFoundException;
import it.prismaprogetti.academy.gestioneauto.core.alimentazione.CodiceAlimentazione;
import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;
import it.prismaprogetti.academy.gestioneauto.core.marca.CodiceMarca;
import it.prismaprogetti.academy.gestioneauto.core.marca.MarcaNotFoundException;
import it.prismaprogetti.academy.gestioneauto.core.marca.MarcaRepository;

public class Modello {

	private CodiceMarca codiceMarca;
	private CodiceModello codiceModello;
	private NomeModello nomeModello;
	private UrlModello urlModello;
	private Map<CodiceAlimentazione, Alimentazione> alimentazioniByCodice = new HashMap<>();

	protected Modello(CodiceMarca codiceMarca, CodiceModello codiceModello, NomeModello nomeModello,
			UrlModello urlModello) {
		super();
		this.codiceMarca = codiceMarca;
		this.codiceModello = codiceModello;
		this.nomeModello = nomeModello;
		this.urlModello = urlModello;
	}

	public CodiceMarca getCodiceMarca() {
		return codiceMarca;
	}

	public CodiceModello getCodiceModello() {
		return codiceModello;
	}

	public NomeModello getNomeModello() {
		return nomeModello;
	}

	public UrlModello getUrlModello() {
		return urlModello;
	}
	
	public Map<CodiceAlimentazione, Alimentazione> getAlimentazioniByCodice() {
		return alimentazioniByCodice;
	}

	public void aggiungiAlimentazione(Alimentazione alimentazione) throws AlimentazioneNotFoundException, AlimentazioneGiaPresenteException {
		
		Alimentazione.checkIsNotNull(alimentazione);
		CodiceAlimentazione codiceAlimentazione = alimentazione.getCodiceAlimentazione();
		
		if ( alimentazioniByCodice.get(codiceAlimentazione) != null ) {
			throw new AlimentazioneGiaPresenteException("alimentazione gi� presente");
		}
		
		alimentazioniByCodice.put(codiceAlimentazione, alimentazione);
		
	}

	public static Modello creazioneModello(CodiceMarca codiceMarca, CodiceModello codiceModello,
			NomeModello nomeModello, UrlModello urlModello, MarcaRepository marcaRepository)
			throws NoSuchParamException, MarcaNotFoundException {

		if (marcaRepository.trovaPerCodice(codiceMarca).isEmpty()) {
			throw new MarcaNotFoundException(codiceMarca.getCodice());
		}

		CodiceModello.checkNotNull(codiceModello);
		NomeModello.checkNotNull(nomeModello);

		return new Modello(codiceMarca, codiceModello, nomeModello, urlModello);

	}

	@Override
	public int hashCode() {
		return Objects.hash(codiceMarca, codiceModello);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Modello other = (Modello) obj;
		return other.getCodiceMarca().equals(this.codiceMarca) && other.getCodiceModello().equals(this.codiceModello);
	}
	
	

}
