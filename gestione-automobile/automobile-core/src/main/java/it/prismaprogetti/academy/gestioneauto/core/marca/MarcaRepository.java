package it.prismaprogetti.academy.gestioneauto.core.marca;

import java.util.List;
import java.util.Optional;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;

public interface MarcaRepository {
	
	void salva(Marca marca) throws MarcaGiaPresenteException, NoSuchParamException;
	Optional<Marca> trovaPerCodice(CodiceMarca codiceMarca) throws NoSuchParamException;
	List<Marca> recupera();

}
