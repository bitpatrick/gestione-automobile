package it.prismaprogetti.academy.gestioneauto.core.marca;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;

public class UrlMarca {

	private String url;

	private UrlMarca(String urlMarca) {
		super();
		this.url = urlMarca;
	}

	public String getUrl() {
		return url;
	}

	@Override
	public int hashCode() {

		return this.getUrl().hashCode();
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}

		if (!(obj instanceof UrlMarca)) {

			return false;
		}

		return ((UrlMarca) obj).getUrl().equals(this.getUrl());
	}

	public static UrlMarca crea(String url) throws NoSuchParamException {

		if (url == null || url.trim().isEmpty()) {
			throw new NoSuchParamException("url nullo");
		}

		return new UrlMarca(url.toUpperCase().trim());
	}

	public static UrlMarca creaOrNull(String url) {

		UrlMarca urlMarca = null;

		try {
			urlMarca = crea(url);

		} catch (NoSuchParamException e) {
		}

		return urlMarca;
	}

}
