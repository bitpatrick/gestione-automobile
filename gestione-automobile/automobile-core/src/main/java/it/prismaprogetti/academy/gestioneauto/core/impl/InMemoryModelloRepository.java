package it.prismaprogetti.academy.gestioneauto.core.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;
import it.prismaprogetti.academy.gestioneauto.core.marca.CodiceMarca;
import it.prismaprogetti.academy.gestioneauto.core.modello.CodiceModello;
import it.prismaprogetti.academy.gestioneauto.core.modello.Modello;
import it.prismaprogetti.academy.gestioneauto.core.modello.ModelloGiaPresenteException;
import it.prismaprogetti.academy.gestioneauto.core.modello.ModelloRepository;

public class InMemoryModelloRepository implements ModelloRepository {

	private Map<CodiceMarca, List<Modello>> listaModelliByCodiceMarca = new HashMap<>();
	private Map<CodiceModello, Modello> modelloByCodiceModello = new HashMap<>();
	
	

	@Override
	public void salva(Modello modello) throws ModelloGiaPresenteException, NoSuchParamException {

		/*
		 * verifico che il modello sia univoco
		 */
		if ( trovaPerCodice(modello.getCodiceModello()).isPresent() ) {
			throw new ModelloGiaPresenteException("modello gi� presente: " + modello.getCodiceModello().getCodice());
		}
		
		/*
		 * indicizzo la lista modelli per codice marca
		 */
		List<Modello> listaModelli = listaModelliByCodiceMarca.get(modello.getCodiceMarca());
		
		if (listaModelli == null ) {
			
			listaModelli = new ArrayList<>();
			listaModelliByCodiceMarca.put(modello.getCodiceMarca(), listaModelli);
		}
		
		/*
		 * aggiungo modello
		 */
		listaModelli.add(modello);
		modelloByCodiceModello.put(modello.getCodiceModello(), modello);
		
	}

	@Override
	public Optional<Modello> trovaPerCodice(CodiceModello codiceModello) throws NoSuchParamException {
		
		CodiceModello.checkNotNull(codiceModello);

		Modello modello = modelloByCodiceModello.get(codiceModello);

		return Optional.ofNullable(modello);
	}

	@Override
	public List<Modello> recupera() {
		
		return new ArrayList<>(modelloByCodiceModello.values());
	}

}
