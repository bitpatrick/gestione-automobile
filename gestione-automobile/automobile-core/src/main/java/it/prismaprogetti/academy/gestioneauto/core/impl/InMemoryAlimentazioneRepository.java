package it.prismaprogetti.academy.gestioneauto.core.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import it.prismaprogetti.academy.gestioneauto.core.alimentazione.Alimentazione;
import it.prismaprogetti.academy.gestioneauto.core.alimentazione.AlimentazioneGiaPresenteException;
import it.prismaprogetti.academy.gestioneauto.core.alimentazione.AlimentazioneNotFoundException;
import it.prismaprogetti.academy.gestioneauto.core.alimentazione.AlimentazioneRepository;
import it.prismaprogetti.academy.gestioneauto.core.alimentazione.CodiceAlimentazione;
import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;

public class InMemoryAlimentazioneRepository implements AlimentazioneRepository {

	Map<CodiceAlimentazione, Alimentazione> alimentazioneByCodice = new HashMap<>();

	@Override
	public void salva(Alimentazione alimentazione)
			throws NoSuchParamException, AlimentazioneNotFoundException, AlimentazioneGiaPresenteException {

		Alimentazione.checkIsNotNull(alimentazione);

		Alimentazione alimentazioneRecuperata = alimentazioneByCodice.get(alimentazione.getCodiceAlimentazione());

		/*
		 * verifico se l'alimentazione recuperata � nulla se lo � allora inserisco
		 * l'alimentazione all'interno del repository
		 */
		if (alimentazioneRecuperata != null) {

			throw new AlimentazioneGiaPresenteException("alimentazione gi� presente nel repository");
		}

		alimentazioneByCodice.put(alimentazione.getCodiceAlimentazione(), alimentazione);

	}

	@Override
	public Optional<Alimentazione> trovaPerCodice(CodiceAlimentazione codiceAlimentazione)
			throws NoSuchParamException, AlimentazioneNotFoundException {

		/*
		 * verifico se il codice alimentazione passato non � null
		 */
		CodiceAlimentazione.checkNotNull(codiceAlimentazione);

		/*
		 * provo a recuperare l'alimentazione dal repository, questa potrebbe essere
		 * null
		 */
		Alimentazione alimentazione = alimentazioneByCodice.get(codiceAlimentazione);

		return Optional.ofNullable(alimentazione);
	}

	@Override
	public List<Alimentazione> recupera() {

		return new ArrayList<>(alimentazioneByCodice.values());
	}

	

}
