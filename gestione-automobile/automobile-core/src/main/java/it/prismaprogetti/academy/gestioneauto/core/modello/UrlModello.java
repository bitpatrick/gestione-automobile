package it.prismaprogetti.academy.gestioneauto.core.modello;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;

public class UrlModello {
	
	private String url;

	private UrlModello(String UrlModello) {
		super();
		this.url = UrlModello;
	}

	public String getUrl() {
		return url;
	}

	@Override
	public int hashCode() {

		return this.getUrl().hashCode();
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}

		if (!(obj instanceof UrlModello)) {

			return false;
		}

		return ((UrlModello) obj).getUrl().equals(this.getUrl());
	}

	public static UrlModello crea(String url) throws NoSuchParamException {

		if (url == null || url.trim().isEmpty()) {
			throw new NoSuchParamException("url nullo");
		}

		return new UrlModello(url.toUpperCase().trim());
	}

	public static UrlModello creaOrNull(String url) {

		UrlModello UrlModello = null;

		try {
			UrlModello = crea(url);

		} catch (NoSuchParamException e) {
		}

		return UrlModello;
	}

}
