package it.prismaprogetti.academy.gestioneauto.core.alimentazione;

import java.util.Objects;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;
import it.prismaprogetti.academy.gestioneauto.core.modello.CodiceModello;

public class AlimentazionePerModello {
	
	private CodiceAlimentazione codiceAlimentazione;
	private CodiceModello codiceModello;
	
	private AlimentazionePerModello(CodiceAlimentazione codiceAlimentazione, CodiceModello codiceModello) {
		super();
		this.codiceAlimentazione = codiceAlimentazione;
		this.codiceModello = codiceModello;
	}

	public CodiceAlimentazione getCodiceAlimentazione() {
		return codiceAlimentazione;
	}

	public CodiceModello getCodiceModello() {
		return codiceModello;
	}

	@Override
	public int hashCode() {
		return Objects.hash(codiceAlimentazione, codiceModello);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlimentazionePerModello other = (AlimentazionePerModello) obj;
		return Objects.equals(codiceAlimentazione, other.codiceAlimentazione)
				&& Objects.equals(codiceModello, other.codiceModello);
	}
	
	public static AlimentazionePerModello crea(CodiceAlimentazione codiceAlimentazione, CodiceModello codiceModello) throws NoSuchParamException {
		
		CodiceAlimentazione.checkNotNull(codiceAlimentazione);
		CodiceModello.checkNotNull(codiceModello);
		
		return new AlimentazionePerModello(codiceAlimentazione, codiceModello);
	}
	
}
