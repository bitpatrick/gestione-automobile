package it.prismaprogetti.academy.gestioneauto.core.alimentazione;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;

public class AlimentazioneReader {

	private final char SEPARATORE_DI_CAMPO;
	private final char MARCATORE_FINE_RIGA;

	protected AlimentazioneReader(char sEPARATORE_DI_CAMPO, char mARCATORE_FINE_RIGA) {
		super();
		SEPARATORE_DI_CAMPO = sEPARATORE_DI_CAMPO;
		MARCATORE_FINE_RIGA = mARCATORE_FINE_RIGA;

	}

	public List<Alimentazione> read(String percorsoDelFile) {

		List<Alimentazione> listaAlimentazioni = new ArrayList<>();

		try (
				
				FileReader fileReader = new FileReader(percorsoDelFile);
				BufferedReader bufferedReader = new BufferedReader(fileReader);
			) {
			
			String riga = null;

			/*
			 * Costrutto che mi permette di ciclare sul file
			 */
			while ((riga = bufferedReader.readLine()) != null) {

				/*
				 * Verificare che la riga abbia il marcatore fine riga ";"
				 */
				if (!riga.endsWith(String.valueOf(MARCATORE_FINE_RIGA))) {
					System.err.println(
							"La riga: " + riga + " verr� scartata perch� sprovvista del seguente marcatore fine riga ["
									+ this.MARCATORE_FINE_RIGA + "]");
					continue;
				}

				String[] rigaArray = riga.split(String.valueOf(SEPARATORE_DI_CAMPO));

				/*
				 * verifico che l'array abbia al suo interno 2 elementi
				 */
				if (rigaArray.length != 2) {
					System.err.println(" L'array ha una lunghezza invalida, per questo verr� scartato");
					continue;
				}
				
				CodiceAlimentazione codiceAlimentazione = CodiceAlimentazione.creaOrNull(rigaArray[0].trim());

				NomeAlimentazione nomeAlimentazione = NomeAlimentazione
						.creaOrNull(rigaArray[1].trim().replaceAll(String.valueOf(MARCATORE_FINE_RIGA), ""));

				Alimentazione alimentazione = Alimentazione.creaAlimentazione(codiceAlimentazione,

						nomeAlimentazione);
				
				listaAlimentazioni.add(alimentazione);
				
			}

		} catch (NoSuchParamException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return listaAlimentazioni;
	}

}
