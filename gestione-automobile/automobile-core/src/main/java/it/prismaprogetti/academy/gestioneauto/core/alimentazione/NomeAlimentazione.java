package it.prismaprogetti.academy.gestioneauto.core.alimentazione;

import it.prismaprogetti.academy.gestioneauto.core.exceptions.NoSuchParamException;

public class NomeAlimentazione {

	private String nome;

	protected NomeAlimentazione(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public static NomeAlimentazione creaOrNull(String nomeInput) {
		
		return new NomeAlimentazione(nomeInput);
		
	}
	
	public static void checkNotNull(NomeAlimentazione nomeAlimentazione) throws NoSuchParamException {

		if (nomeAlimentazione == null) {
			throw new NoSuchParamException("il nome alimentazione � nullo");
		}

	}
	
	@Override
	public int hashCode() {
		return this.nome.hashCode();
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}

		if (!(obj instanceof NomeAlimentazione)) {
			return false;
		}

		return ((NomeAlimentazione) obj).nome.equals(this.nome);
	}

}
